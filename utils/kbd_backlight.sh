#!/bin/sh

if [ $# -eq 0 ]; then exit 1; fi
	bright_file="/sys/devices/platform/applesmc.768/leds/smc::kbd_backlight/brightness"
	mbright_file="/sys/devices/platform/applesmc.768/leds/smc::kbd_backlight/max_brightness"

	while read line; do
		brightness=$line
	done < "$bright_file"

	while read line; do
		max_brightness=$line
	done < "$mbright_file"

	step=1
	if [ $# -gt 1 ]; then
		step=$2
	fi
	declare -i brightness
	if [ $1 = "-" ]; then
		if [ $brightness -ne 0 ]; then
			brightness=$brightness-$step;
			echo $brightness > $bright_file
		fi
	else
		if [ $brightness -ne $max_brightness ]; then
			brightness=$brightness+$step;
			echo $brightness > $bright_file
	fi
fi
