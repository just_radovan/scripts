#!/bin/sh

ACPI=`acpi -b`

if [[ $ACPI == *Charging*  ]]; then
	TIME=`echo $ACPI | sed -r 's/.* ([0-9]+):([0-9]+):([0-9]+) until charged.*/\1h\2/'`" ↗"
elif [[ $ACPI == *Discharging* ]]; then
	TIME=`echo $ACPI | sed -r 's/.* ([0-9]+):([0-9]+):([0-9]+) remaining.*/\1h\2/'`" ↘"
else
	TIME=`echo $ACPI | sed -r 's/.* ([0-9]+).*/\1%/'`
fi

echo "$TIME"
